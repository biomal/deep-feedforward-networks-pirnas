# -*- coding: utf-8 -*-

import os
import sys
import shutil
import argparse
import subprocess

import numpy  as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

def extract_kmers(filename):
    prefix = filename[slice(0, 3)]
    for k in range(1, 5):
        command = [
            'python '
            'Pse-in-One-2.0/nac.py '
            '{filename} DNA kmer '
            '-k {k} -f csv -out tmp/{prefix}_k{k}.csv'.format(filename=filename,
                                                              k=k,
                                                              prefix=prefix)
        ]
        subprocess.call(command, shell=True)


def extract_mismatch(filename):
    prefix = filename[slice(0, 3)]
    for k, m in zip([1, 2, 3, 4], [0, 1, 1, 1]):
        command = [
            'python '
            'Pse-in-One-2.0/nac.py '
            '{filename} DNA mismatch '
            '-m {m} -k {k} -f csv -out tmp/{prefix}_m{k}.csv'.format(filename=filename,
                                                                     m=m,
                                                                     k=k,
                                                                     prefix=prefix)
        ]
        subprocess.call(command, shell=True)


def extract_subsequence(filename):
    prefix = filename[slice(0, 3)]
    for k in range(1, 5):
        command = [
            'python '
            'Pse-in-One-2.0/nac.py '
            '{filename} DNA subsequence '
            '-k {k} -f csv -out tmp/{prefix}_s{k}.csv'.format(filename=filename,
                                                              k=k,
                                                              prefix=prefix)
        ]
        subprocess.call(command, shell=True)


def extract_features(filename):
    print('Extracting k-mers (k from 1 to 4)')
    extract_kmers(filename)
    print('Extracting mismatch (k from 1 to 4, m from 0 to 1)')
    extract_mismatch(filename)
    print('Extracting subsequence (k from 1 to 4)')
    extract_subsequence(filename)


def getFiles(feature):
    files = os.listdir('tmp/')
    pos_files = list()
    neg_files = list()
    for file in files:
        prefix = file[0:3]
        letter = file[4]
        if letter == feature:
            if prefix == 'pos':
                pos_files.append(file)
                pos_files.sort()
            elif prefix == 'neg':
                neg_files.append(file)
                neg_files.sort()
            else:
                raise ValueError('Prefixo {prefix} não definido!'.format(prefix=prefix))
    
    return pos_files, neg_files


def mergeHorizontally(files, label):
    data = list()
    for file in files:
        df = pd.read_csv('tmp/' + file, header=None, sep=None, engine='python')
        data.append(df)
    df = pd.concat(data, axis=1)
    n_rows, n_cols = df.shape
    df.insert(n_cols, n_cols, label)
    
    return df


def mergeVertically(pos_data, neg_data):
    return pd.concat([pos_data, neg_data], axis=0)


def mergeFiles(feature):
    if feature == 'kmers':
        pos_files, neg_files = getFiles('k')
    elif feature == 'mismatch':
        pos_files, neg_files = getFiles('m')
    elif feature == 'subsequence':
        pos_files, neg_files = getFiles('s')
    else:
        raise ValueError('Feature {feature} not identified!'.format(feature=feature))
        
    pos_data = mergeHorizontally(pos_files, 1)
    neg_data = mergeHorizontally(neg_files, 0)
    data = mergeVertically(pos_data, neg_data)
        
    return data


def standardscaler(dataset):
    X = dataset.iloc[:,:-1]
    Y = dataset.iloc[:,-1:]
    scaler = StandardScaler().fit_transform(X)
    xy = np.hstack((scaler, Y))
    return pd.DataFrame(xy)


def minmaxscaler(dataset):
    X = dataset.iloc[:,:-1]
    Y = dataset.iloc[:,-1:]
    scaler = MinMaxScaler().fit_transform(X)
    xy = np.hstack((scaler, Y))
    return pd.DataFrame(xy)



def do_extraction(args):
    DIR = 'Pse-in-One-2.0'
    ZIPFILE = 'Pse-in-One-2.0.zip'

    if os.path.exists(DIR):
        print('Diretório {dir} encontrado'.format(dir=DIR))
    else:
        if os.path.exists(ZIPFILE):
            print('Arquivio {zip} encontrado. Extraindo...'.format(zip=ZIPFILE))
            subprocess.call(['unzip', ZIPFILE])
            print('Extração completa!')
        else:
            print('Não foi possível encontrar o diretório {dir} ou o arquivo {zip}')
            print('Encerrando a execução do programa')
            sys.exit()

    POS_FILE = args.pos_samples
    NEG_FILE = args.neg_samples
    
    if not os.path.exists(POS_FILE):
        print('Positive samples (pos) not found!'.format(pos=POS_FILE))
        print('Aborting')
        sys.exit()
    
    if not os.path.exists(NEG_FILE):
        print('Negative samples (neg) not found!'.format(neg=NEG_FILE))
        print('Aborting')
        sys.exit()

    print('Creating temporary directory for operation')
    try:
        os.makedirs('tmp')
    except OSError:
        print('temp directory already exists. All files in temp will be deleted')
        shutil.rmtree('tmp')
        os.makedirs('tmp')

    print('Extracting features from positive samples')
    extract_features(POS_FILE)

    print('Extracting features from negative samples')
    extract_features(NEG_FILE)

    features = ['kmers', 'mismatch', 'subsequence']
    for feature in features:
        data = mergeFiles(feature)
        standardized_data = standardscaler(data)
        normalized_data   = minmaxscaler(data)
        data.to_csv(feature + '-raw.csv', index=False, header=False, float_format='%.5f')
        standardized_data.to_csv(feature + '-standard.csv', index=False, header=False, float_format='%.5f')
        normalized_data.to_csv(feature + '-normal.csv', index=False, header=False, float_format='%.5f')
    
    print('Deleting temporary files')
    shutil.rmtree('tmp')
    print('Feature extraction completed!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-p',
                        '--pos',
                        type=str,
                        action='store',
                        dest='pos_samples',
                        required=True)

    parser.add_argument('-n',
                        '--neg',
                        type=str,
                        action='store',
                        dest='neg_samples',
                        required=True)
    
    args = parser.parse_args()
    do_extraction(args)

