import numpy as np
import tensorflow as tf
from sklearn.metrics import f1_score, recall_score, confusion_matrix


def get_metrics(n_thresholds=200, threshold=0.5):
    auc = tf.keras.metrics.AUC(num_thresholds=n_thresholds, name='auc')
    sen = tf.keras.metrics.Recall(thresholds=threshold, name='sensivity')
    pre = tf.keras.metrics.Precision(thresholds=threshold, name='precision')
    
    return [auc, sen, pre]


def get_f1_score(y_true, y_pred, threshold=0.5):
    y_pred = np.where(np.array(y_pred) > threshold, 1, 0)
    f1 = f1_score(y_true,
		  y_pred,
		  pos_label=1,
		  zero_division=0,
		  average='binary')
    return f1


def get_binary_specificity(y_true, y_pred, threshold=0.5):
    y_pred = np.where(np.array(y_pred) > threshold, 1, 0)
    spc = recall_score(y_true, 
                       y_pred, 
                       pos_label=0, 
                       zero_division=0, 
                       average='binary')

    return spc


def get_binary_confusion_matrix(y_true, y_pred, threshold=0.5):
    y_pred = np.where(np.array(y_pred) > threshold, 1, 0)
    cm = confusion_matrix(y_true, y_pred)
    
    return cm
