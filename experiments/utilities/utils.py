import os
import sys
import logging
import argparse
from datetime import datetime

import numpy  as np
import pandas as pd


def get_parser():
    parser = argparse.ArgumentParser(description='', allow_abbrev=False)
    parser.add_argument('-f',
                        '--file',
                        type=str,
                        action='store', 
                        dest='input_file',
                        required=True, 
                        help='Path to input file')
    
    parser.add_argument('-d',
                        '--dir',
                        type=str,
                        action='store', 
                        dest='destination', 
                        required=False, 
                        default=None,
                        help='Path to output directory')
    
    parser.add_argument('-k',
                        '--kfolds',
                        type=int,
                        action='store', 
                        dest='kfolds', 
                        required=False, 
                        default=10,
                        help='Path to output directory')
    
    parser.add_argument('-l',
                         '--layers',
                         type=int,
                         choices=[3, 5],
                         action='store',
                         dest='n_layers',
                         required=True,
                         help='Neural network number of layers')
    
    parser.add_argument('-a',
                         '--activation',
                         type=str,
                         choices=['relu', 'sigmoid'],
                         action='store',
                         dest='activation',
                         required=True,
                         help='Neural network activation function')
    
    parser.add_argument('-o',
                         '--optimizer',
                         type=str,
                         choices=['adam', 'sgd'],
                         action='store',
                         dest='optimizer',
                         required=True,
                         help='Neural network optimizer')
    
    return parser


def set_folder(destination, layers, activation, optimizer):
    today = datetime.now().strftime('%Y%m%d')
    directory = 'DFN{layers}-{activation}-{optimizer}-{date}'.format(layers=layers,
                                                                     activation=activation,
                                                                     optimizer=optimizer,
                                                                     date=today)
    if destination is None:
        destination = os.path.join(os.getcwd(), directory)
        os.makedirs(destination, exist_ok=True)
        return destination
    else:
        destination = os.path.join(destination, directory)
        os.makedirs(destination, exist_ok=True)
        return destination


def set_logger(destination):
    today = datetime.now().strftime('%Y%m%d')
    filename = '{root}/{date}.{ext}'.format(root=destination, date=today, ext='log')
    
    file_handler   = logging.FileHandler(filename=filename)
    stdout_handler = logging.StreamHandler(sys.stdout)
    
    logging.basicConfig(
        level=logging.DEBUG, 
        format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s: %(message)s',
        handlers=[
            file_handler,
            stdout_handler
        ]
    )
    return logging.getLogger('LOGGER')


def get_data(inputfile):
    dataset = np.loadtxt(inputfile, delimiter=',')
    np.random.RandomState(seed=0).shuffle(dataset)
    X, Y = dataset[:,:-1], dataset[:,-1]
    
    return X, Y


def save_results(scores, metrics_names, destination):
    filename = '{root}/results.csv'.format(root=destination)

    df = pd.DataFrame([scores], columns=metrics_names)
    df.to_csv(filename, index=False, float_format='%.5f', mode='a')


def save_confusion_matrix(confusion_matrix, fold, destination):
    filename = '{root}/confusion_matrix_{fold}'.format(root=destination, fold=fold)

    df = pd.DataFrame(confusion_matrix)
    df.to_csv(filename)


def save_history(history, fold, destination):
    filename = '{root}/learning_curve_{fold}'.format(root=destination, fold=fold)

    with open(filename, 'a') as f:
            f.write(', '.join(map(str, history.history['loss'])) + '\n')
            f.write(', '.join(map(str, history.history['val_loss'])) + '\n')