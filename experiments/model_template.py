import os
import time
import random
from datetime import datetime

import numpy  as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import StratifiedKFold

from utilities.utils import get_parser, set_folder, set_logger, get_data
from utilities.utils import save_results, save_confusion_matrix, save_history
from utilities.metrics import get_metrics, get_f1_score, get_binary_specificity, get_binary_confusion_matrix


SEED_VALUE = 0

os.environ['PYTHONHASHSEED']=str(SEED_VALUE)
random.seed(SEED_VALUE)
np.random.seed(SEED_VALUE)
tf.random.set_seed(SEED_VALUE)


def build_model(input_dim, n_layers, func, optimizer, metrics):
    init = 'he_normal' if func == 'relu' else 'glorot_normal'

    model = tf.keras.models.Sequential()
    model.add(
        tf.keras.layers.Dense(
            input_dim=input_dim, 
            units=340, 
            activation=func, 
            kernel_initializer=init
        )
    )
    model.add(tf.keras.layers.Dropout(0.5))

    for i in range(1, n_layers):
        model.add(
            tf.keras.layers.Dense(
                units=340,
                activation=func,
                kernel_initializer=init,
            )
        )
        model.add(tf.keras.layers.Dropout(0.5))

    model.add(
        tf.keras.layers.Dense(
            units=1, 
            activation='sigmoid', 
            kernel_initializer='glorot_normal'
        )
    )
    
    model.compile(optimizer = optimizer,
                  loss      = 'binary_crossentropy',
                  metrics   = ['binary_accuracy'] + metrics)
    
    return model


def main(args):
    INPUT_FILE = args.input_file
    DESTINATION = args.destination
    N_FOLDS = args.kfolds
    N_LAYERS = args.n_layers
    FUNC = args.activation
    OPTIMIZER = args.optimizer
    
    root   = set_folder(DESTINATION, N_LAYERS, FUNC, OPTIMIZER)
    logger = set_logger(root)

    logger.info('Importing file {}'.format(os.path.abspath(INPUT_FILE)))
    X, Y = get_data(INPUT_FILE)
    logger.info('File imported')

    N_SAMPLES, N_FEATURES = X.shape
    EPOCHS = 128
    BATCH_SIZE = 32

    metrics = get_metrics(n_thresholds=N_SAMPLES)
    kFold = StratifiedKFold(n_splits=N_FOLDS)
    logger.info('Starting {folds}-Folds Cross Validation'.format(folds=N_FOLDS))
    for idx, (train, test) in enumerate(kFold.split(X, Y)):
        N_TRAINING_SAMPLES = len(X[train])
        N_TEST_SAMPLES = len(X[test])

        logger.info('Fold {idx}'.format(idx=idx))
        logger.info('Building model with '
                    '{n_layers} layers, {func} activation and {optimizer} optimizer'.format(n_layers=N_LAYERS,
                                                                                            func=FUNC,
                                                                                            optimizer=OPTIMIZER))
        model   = build_model(N_FEATURES, N_LAYERS, FUNC, OPTIMIZER, metrics)
        logger.info('Built model')
        
        logger.info('Training model with '
                    '{n_samples} samples, {epochs} epochs and {batch} of batch_size'.format(n_samples=N_TRAINING_SAMPLES,
                                                                                            epochs=EPOCHS,
                                                                                            batch=BATCH_SIZE))                                                                                
        history = model.fit(X[train], Y[train], epochs=EPOCHS, batch_size=BATCH_SIZE, validation_split=0.2, verbose=0)
        logger.info('Training finished')

        logger.info('Evaluating model with {n_samples} samples'.format(n_samples=N_TEST_SAMPLES))
        scores  = model.evaluate(X[test], Y[test], verbose=0)

        y_pred = (model.predict(X[test]) > 0.5).astype('int32')
        metrics_names = model.metrics_names.copy()
        scores.append(get_f1_score(Y[test], y_pred))
        metrics_names.append('f1_score')
        scores.insert(4, get_binary_specificity(Y[test], y_pred))
        metrics_names.insert(4, 'specificity')
        logger.info('Model evaluated')

        logger.info('Saving results')
        save_results(scores, metrics_names, root)

        confusion_matrix = get_binary_confusion_matrix(Y[test], y_pred)
        save_confusion_matrix(confusion_matrix, idx, root)
        
        save_history(history, idx, root)


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()

    print('Starting process!')
    start_time = time.time()
    main(args)
    minutes = (time.time() - start_time) / 60
    seconds = (time.time() - start_time) % 60
    print('Process completed!\n'
          'Time: {:.0f} minute(s) and {:.0f} second(s)'.format(minutes, seconds))

